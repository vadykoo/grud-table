<?php

namespace App\Http\Controllers;
use App\Invoices;
use Illuminate\Http\Request;

class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Invoices $invoice)
    {
//        $users = DB::table('users')->paginate(15);
//
//        return view('user.index', ['users' => $users]);

        $invoices = $invoice->paginate(10);
        return view('invoices.index', compact('invoices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Invoices $invoice, Request $request)
    {
        $i = $invoice->create($request->all());
//        $i = $invoice->where('id', $request->id)->first();
//        $i->update($request->all());

            $response = array(
                'status' => 'success',
                'msg' => $i,
            );
            return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Invoices $invoice, Request $request)
    {
        $i = $invoice->where('id', $request->id)->first();
        $i->update(array($request->field => $request->value));
        $response = array(
            'status' => 'success',
            'msg' => $i,
        );
        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Invoices $invoice,  Request $request)
    {
        $u = $invoice->where('id', $request->id)->first();
        $u->delete();

        $response = array(
            'status' => 'success',
            'msg' => $request->id,
        );
        return response()->json($response);
    }
}
