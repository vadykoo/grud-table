<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoices extends Model
{
    protected $fillable = ['Category', 'Vendor', 'Customer', 'Status', 'Price'];
}
