@extends('layouts.panel')
@section('panel')
    <style>
        td{
            word-break: break-all;

        }
    </style>
    <script>
    // Save data
  /*  $(".edit").on('keyup', function () {
        console.log(123);
        $(this).removeClass("editMode");
        var id = this.id;
        var split_id = id.split("_");
        var field_name = split_id[0];
        var edit_id = split_id[1];
        var value = $(this).text();
        $.ajax({
            url: '/update-invoices',
            type: 'post',
            data: {_token: CSRF_TOKEN, field: field_name, value: value, id: edit_id},
            dataType: 'JSON',
            success: function (response) {
                console.log(response);
            }
        });
    });*/
    $(document).ready(function() {
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

        $(document).on('click', '.edit', function () {
            $(this).addClass('editMode');
            $(this).on('focusout', function () {
                $(this).removeClass("editMode");
                var id = this.id;
                var split_id = id.split("_");
                var field_name = split_id[0];
                var edit_id = split_id[1];
                var value = $(this).text();
                $.ajax({
                    url: '/update-invoices',
                    type: 'post',
                    data: {_token: CSRF_TOKEN, field: field_name, value: value, id: edit_id},
                    dataType: 'JSON',
                    success: function (response) {
                        console.log(response);
                    }
                });
            });
        });

        $(".add").click(function(){
            $.ajax({
                url: '/',
                type: 'POST',
                data: {_token: CSRF_TOKEN, Category:$("#Category_form").val(), Vendor:$("#Vendor_form").val(), Customer:$("#Customer_form").val(), Status:$("#Status_form").val(), Price:$("#Price_form").val()},
                dataType: 'JSON',
                success: function (data) {
                    $(".new").after('<tr class="table-row" style="cursor: pointer" id="'+data.msg.id+'">' +
                        '<th>'+data.msg.id+'</th>' +
                        '<th contentEditable="true" class="edit" id="Category_'+data.msg.id+'">'+data.msg.Category+'</th>' +
                        '<th contentEditable="true" class="edit" id="Vendor_'+data.msg.id+'">'+data.msg.Vendor+'</th>' +
                        '<th contentEditable="true" class="edit" id="Customer_'+data.msg.id+'">'+data.msg.Customer+'</th>' +
                        '<th contentEditable="true" class="edit" id="Status_'+data.msg.id+'">'+data.msg.Status+'</th>' +
                        '<th contentEditable="true" class="edit" id="Price_'+data.msg.id+'">'+data.msg.Price+'</th>' +
                        '<th><button type="submit" class="deletebutton btn btn-danger" value="'+data.msg.id+'">Delete</button></th>'+
                        '<tr>');
                    console.log(data);
                }
            });
        });

        $(document).on('click', '.deletebutton', function(){
            var number = $(this).val();
            $.ajax({
                url: '/delete',
                type: 'POST',
                data: {_token: CSRF_TOKEN, id:$(this).val()},
                dataType: 'JSON',
                success: function (data) {
                    $("#"+number).remove();
                    console.log(data);
                }
            });
        });
    });
    </script>
<div class="panel-heading container-fluid">
            <div class="container">
                <div class="row">
                        <table class="table table-bordered table-responsive table-striped table-hover ">
                            <tr class="new">
                                <th >ID</th>
                                <th width="10%">Category</th>
                                <th width="25%">Vendor</th>
                                <th width="15%">Customer</th>
                                <th width="15%">Status</th>
                                <th width="10%">Price</th>
                                <th width="25%">Actions</th>
                            </tr>
                            @php
                                $count=0;
                                echo $invoices;
                                foreach ($invoices as $invoice){
                                    $count++;
                                    $array_Category[$count] = $invoice->Category;
                                    $array_Vendor[$count] = $invoice->Vendor;
                                    $array_Customer[$count] = $invoice->Customer;
                                    $array_Status[$count] = $invoice->Status;
                                }
                            @endphp
@foreach ($invoices as $invoice)
    <tr class="table-row" style="cursor: pointer" id="{{$invoice->id}}">
        <td>{{$invoice->id}}</td>
        <td contentEditable='true' class='edit' id='Category_{{$invoice->id}}'>{{$invoice->Category}}</td>
        <td contentEditable='true' class='edit' id='Vendor_{{$invoice->id}}'>{{$invoice->Vendor}}</td>
        <td contentEditable='true' class='edit' id='Customer_{{$invoice->id}}'>{{$invoice->Customer}}</td>
        <td contentEditable='true' class='edit' id='Status_{{$invoice->id}}'>{{$invoice->Status}}</td>
        <td contentEditable='true' class='edit' id='Price_{{$invoice->id}}'>{{$invoice->Price}}</td>
        <td>
            <button type="submit" class="deletebutton btn btn-danger" value="{{$invoice->id}}">Delete</button>
        </td>
    </tr>
    @endforeach
                        <tr style="background: #1bff0026">
                            <td>
                            </td>
                            <td>
                                <input id="Category_form" list="Category" required>
                                    <datalist id="Category">
                                        @if(isset($array_Category))
                                    @foreach (array_unique($array_Category) as $a)
                                        <option  value={{$a}}>{{$a}}</option>
                                    @endforeach
                                            @endif
                                </datalist>
                            </td>
                            <td>
                                <input id="Vendor_form" list="Vendor" required>
                                <datalist  id="Vendor">
                                    @if(isset($array_Vendor))
                                    @foreach (array_unique($array_Vendor) as $a)
                                        <option  value={{$a}}>{{$a}}</option>
                                    @endforeach
                                    @endif

                                </datalist>
                            </td>
                            <td>
                                <input id="Customer_form" list="Customer" required>
                                <datalist  id="Customer">
                                    @if(isset($array_Customer))
                                    @foreach (array_unique($array_Customer) as $a)
                                        <option  value={{$a}}>{{$a}}</option>
                                    @endforeach
                                    @endif
                                </datalist>
                            </td>
                            <td>
                                <input id="Status_form" list="Status" required>
                                <datalist  id="Status">
                                    @if(isset($array_Status))
                                    @foreach (array_unique($array_Status) as $a)
                                        <option  value={{$a}}>{{$a}}</option>
                                    @endforeach
                                    @endif

                                </datalist>
                            </td>
                            <td>
                            <input type="number" id="Price_form" name="Price" required/>
                            </td>
                            <td>
                            <button type="submit" class="add btn btn-success">Add</button>
                            </td>
                        </tr>
                        </table>
    </div>
    </div>
    </div>
    </div>
   {{ $invoices->links() }}
@endsection